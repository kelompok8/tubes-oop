package com.tubes.CLI;

import java.awt.Color;

import org.apache.commons.lang3.StringUtils;

import com.tubes.Core.CONTROL;
import com.tubes.Core.CONTROL.CONSOLE;

import enigma.console.Console;
import enigma.console.TextAttributes;
import enigma.core.Enigma;

public class Konsol {
	private static int width = CONTROL.CONSOLE.WIDTH - 1;
	public static Console enigma;
	static {
		Konsol.enigma = Enigma.getConsole("MINIROBOTWARS!!");
		TextAttributes attrs = new TextAttributes(Color.ORANGE.brighter(),
				Color.BLACK);
		Konsol.enigma.setTextAttributes(attrs);
		CONTROL.CURRENT_POINTER = CONTROL.CURRENT_PLAYER.getName()
				.toUpperCase();
	}

	public static void wait(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {

		}
	}

	public static void center(String s) {
		System.out.println(StringUtils.center(s, Konsol.width));
	}

	public static void center(String s, int size) {
		System.out.println(StringUtils.center(s, size));
	}

	public static void reset() {
		Konsol.color(CONSOLE.Fg, CONSOLE.Bg);
	}

	public static String toCenter(String s) {
		return StringUtils.center(s, Konsol.width);
	}

	public static void color(Color color) {
		Konsol.enigma.setTextAttributes(new TextAttributes(color));

	}

	public static void color(Color text, Color background) {
		Konsol.enigma.setTextAttributes(new TextAttributes(text, background));
	}

	public static void random() {
		float h = (float) Math.random();
		float s = (float) Math.random();
		float b = (float) Math.random();
		Color color = Color.getHSBColor(h, s, b);
		Color color2 = Color.getHSBColor(b, s, h);
		Konsol.enigma.setTextAttributes(new TextAttributes(color, color2));
	}

	public static void error(String Input) {
		if (Konsol.enigma.getTextAttributes().getForeground() == Color.RED)
			Konsol.color(Color.WHITE);
		else
			Konsol.color(Color.RED);
		System.err.append("\n	" + Input.split(" ")[0]
				+ " is not an ACCEPTED COMMAND\n");
	}

	public static void error(String Input, String[] commands) {
		if (Konsol.enigma.getTextAttributes().getForeground() == Color.RED)
			Konsol.color(Color.WHITE);
		else
			Konsol.color(Color.RED);
		System.err.append("\n	" + Input.split(" ")[0]
				+ " is not an ACCEPTED COMMAND\n");
		for (String command : commands)
			System.out.print("	>" + command);
		System.out.println("\n");
	}

	public static void error(String Input, Class<?> c) {
		if (Konsol.enigma.getTextAttributes().getForeground() == Color.RED)
			Konsol.color(Color.WHITE);
		else
			Konsol.color(Color.RED);
		System.err.append("\n	" + Input.split(" ")[0]
				+ " is not an ACCEPTED COMMAND\n");
		for (Object current : c.getEnumConstants())
			if (current.toString() != "ERROR")
				System.out.print("	>" + current);
		System.out.println("\n");
	}




}
