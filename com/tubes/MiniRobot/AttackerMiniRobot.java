package com.tubes.MiniRobot;

public abstract class AttackerMiniRobot extends MiniRobot implements Attacker {

	protected AttackerMiniRobot(int x, int y) {
		super(x, y);
	}

	private Attacker i;

	public void diagonal() {
		i = new AttackerDiagonal();
	}

	@Override
	public abstract int getAttack();

	@Override
	public abstract int getDefend();

	public void horizontal() {
		i = new AttackerHorizontal();
	}

	public AttackDirection shoot() {
		return i.shoot();
	}

	public void vertical() {
		i = new AttackerVertical();
	}

}
