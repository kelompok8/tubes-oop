package com.tubes.MiniRobot;


public abstract class DefenderMiniRobot extends MiniRobot implements Defender {

	protected DefenderMiniRobot(int x, int y) {
		super(x, y);
	}

	@Override
	public void repair() {

	}

}
