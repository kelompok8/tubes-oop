package com.tubes.MiniRobot;

import java.util.ArrayList;

import com.tubes.Core.Robot;

public abstract class MiniRobot extends Robot {

	public MiniRobotInformation information = new MiniRobotInformation
	/* Start Builder */
	.Builder().name(getName()).type(getType()).habit(getHabit())
			.attack(getAttack()).defend(getDefend()).cost(getCost())
			.energy(getEnergy()).playable(isPlayable()).build();
	
	/* Finish Builder */

	public abstract int getAttack();

	public abstract int getDefend();

	public abstract int getEnergy();

	public abstract int getCost();

	public abstract boolean isPlayable();

	public String getHabit() {
		try {
			return getClass().getInterfaces()[0].getSimpleName();
		} catch (ArrayIndexOutOfBoundsException a) {
			return "Land";
		}
	}

	public String getType() {

		return getClass().getSuperclass().getInterfaces()[0].getSimpleName();
	}

	public static ArrayList<MiniRobotInformation> types = new ArrayList<MiniRobotInformation>();

	protected MiniRobot() {
		super();
	}

	protected MiniRobot(int x, int y) {
		super(x, y, 1, 1);
	}

	public void printInformation() {
		information.printInformation();
	}

	public void setLocation(int x, int y) {
		getBound().setLocation(x, y);
	}

}