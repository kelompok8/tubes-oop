package com.tubes.MiniRobot;

class AttackerDiagonal implements Attacker {
	@Override
	public AttackDirection shoot() {
		return AttackDirection.DIAGONAL;
	}
}