package com.tubes.MiniRobot;

import com.tubes.Core.Entity;
import com.tubes.Core.Movable;

public class Bullet extends Entity implements Movable {

	private int damage;

	protected Bullet(int damage) {
		super();
		this.damage = damage;
	}

	public int getDamage() {
		return damage;
	}

	@Override
	public void moveAmount(int dx, int dy) {
		getBound().setLocation((int) getBound().getX() + dx,
				(int) getBound().getY() + dy);
	}

	@Override
	public void moveTo(int x, int y) {
		getBound().setLocation(x, y);

	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public void setLocation(int x, int y) {
	}

}
