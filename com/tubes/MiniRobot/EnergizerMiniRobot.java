package com.tubes.MiniRobot;

import java.util.HashMap;
import java.util.Map;

import com.tubes.Player.Energy;

public abstract class EnergizerMiniRobot extends MiniRobot implements Energizer {

	protected EnergizerMiniRobot(int x, int y) {
		super(x, y);
	}

	@Override
	public Energy genEnergy() {
		int random;
		random = (int) (Math.random() * 7) + 1;
		Map<Integer, Integer> Randomizer = new HashMap<Integer, Integer>();
		Randomizer.put(1, 5);
		Randomizer.put(2, 5);
		Randomizer.put(3, 10);
		Randomizer.put(4, 10);
		Randomizer.put(5, 10);
		Randomizer.put(6, 10);
		Randomizer.put(7, 10);
		Randomizer.put(8, 20);
		return new Energy(Randomizer.get(random));
	}
}
