package com.tubes.MiniRobot.type;

import com.tubes.Core.Airborne;
import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.ENERGY;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.PRICE;
import com.tubes.MiniRobot.DefenderMiniRobot;
import com.tubes.MiniRobot.MiniRobot;

public class Perisai extends DefenderMiniRobot implements Airborne {
	public static final int ID = 6;
	public static int attack = ATTACK.NULL;
	public static int cost = PRICE.MEDIUM;
	public static int defend = HEALTH.HIGH;
	public static int energy = ENERGY.CHEAP;

	private static boolean playable;
	private int health = Perisai.defend;

	public void setHealth(int health) {
		this.health = health;
	}

	public Perisai() {
		super(0, 0);
		MiniRobot.types.add(information);
	}

	public Perisai(int x, int y) {
		super(x, y);
		MiniRobot.types.add(information);
	}

	public static void printInfo() {
		new Perisai().printInformation();
	}

	/* Getter id */
	public static int getId() {
		return Perisai.ID;
	}

	/* Getter attack */
	public int getAttack() {
		return Perisai.attack;
	}

	/* Getter cost */
	public int getCost() {
		return Perisai.cost;
	}

	/* Getter defend */
	public int getDefend() {
		return Perisai.defend;
	}

	/* Getter energy */
	public int getEnergy() {
		return Perisai.energy;
	}

	/* Getter playable */
	public boolean isPlayable() {
		return Perisai.playable;
	}

	/* Getter health */
	public int getHealth() {
		return health;
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub

	}
}
