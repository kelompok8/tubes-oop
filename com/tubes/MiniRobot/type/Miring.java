package com.tubes.MiniRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.ENERGY;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.PRICE;
import com.tubes.MiniRobot.AttackerMiniRobot;
import com.tubes.MiniRobot.MiniRobot;

public class Miring extends AttackerMiniRobot {
	public static final int ID = 4;
	public static int attack = ATTACK.LOW;
	public static int cost = PRICE.CHEAP;
	public static int defend = HEALTH.MEDIUM;
	public static int energy = ENERGY.CHEAP;

	private static boolean playable;
	private int health = Miring.defend;

	public void setHealth(int health) {
		this.health = health;
	}

	public Miring() {
		super(0, 0);
		super.diagonal();
		MiniRobot.types.add(information);
	}

	public Miring(int x, int y) {
		super(x, y);
		super.diagonal();
		MiniRobot.types.add(information);
	}

	public static void printInfo() {
		new Miring().printInformation();
	}

	/* Getter id */
	public static int getId() {
		return Miring.ID;
	}

	/* Getter attack */
	public int getAttack() {
		return Miring.attack;
	}

	/* Getter cost */
	public int getCost() {
		return Miring.cost;
	}

	/* Getter defend */
	public int getDefend() {
		return Miring.defend;
	}

	/* Getter energy */
	public int getEnergy() {
		return Miring.energy;
	}

	/* Getter playable */
	public boolean isPlayable() {
		return Miring.playable;
	}

	/* Getter health */
	public int getHealth() {
		return health;
	}
}