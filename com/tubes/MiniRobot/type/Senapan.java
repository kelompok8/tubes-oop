package com.tubes.MiniRobot.type;

import com.tubes.Core.Airborne;
import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.ENERGY;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.PRICE;
import com.tubes.MiniRobot.AttackerMiniRobot;
import com.tubes.MiniRobot.MiniRobot;

public class Senapan extends AttackerMiniRobot implements Airborne {
	public static final int ID = 7;
	public static int attack = ATTACK.MEDIUM;
	public static int cost = PRICE.MEDIUM;
	public static int defend = HEALTH.MEDIUM;
	public static int energy = ENERGY.MEDIUM;
	private static boolean playable;
	private int health = Senapan.defend;

	public void setHealth(int health) {
		this.health = health;
	}

	public Senapan() {
		super(0, 0);
		super.horizontal();
		MiniRobot.types.add(information);
	}

	public Senapan(int x, int y) {
		super(x, y);
		super.horizontal();
		MiniRobot.types.add(information);
	}

	public static void printInfo() {
		new Senapan().printInformation();
	}

	/* Getter id */
	public static int getId() {
		return Senapan.ID;
	}

	/* Getter attack */
	public int getAttack() {
		return Senapan.attack;
	}

	/* Getter cost */
	public int getCost() {
		return Senapan.cost;
	}

	/* Getter defend */
	public int getDefend() {
		return Senapan.defend;
	}

	/* Getter energy */
	public int getEnergy() {
		return Senapan.energy;
	}

	/* Getter playable */
	public boolean isPlayable() {
		return Senapan.playable;
	}

	/* Getter health */
	public int getHealth() {
		return health;
	}

	@Override
	public void fly() {
		// TODO Auto-generated method stub

	}
}