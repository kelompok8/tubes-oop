package com.tubes.MiniRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.ENERGY;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.PRICE;
import com.tubes.MiniRobot.AttackerMiniRobot;
import com.tubes.MiniRobot.MiniRobot;

public class Meriam extends AttackerMiniRobot {
	public static final int ID = 1;
	public static int attack = ATTACK.LOW;
	public static int cost = PRICE.CHEAP;
	public static int defend = HEALTH.MEDIUM;
	public static int energy = ENERGY.CHEAP;

	private static boolean playable;
	private int health = Meriam.defend;

	public void setHealth(int health) {
		this.health = health;
	}

	public Meriam() {
		super(0, 0);
		super.horizontal();
		MiniRobot.types.add(information);
	}

	public Meriam(int x, int y) {
		super(x, y);
		super.horizontal();
		MiniRobot.types.add(information);
	}

	public static void printInfo() {
		new Meriam().printInformation();
	}

	/* Getter id */
	public static int getId() {
		return Meriam.ID;
	}

	/* Getter attack */
	public int getAttack() {
		return Meriam.attack;
	}

	/* Getter cost */
	public int getCost() {
		return Meriam.cost;
	}

	/* Getter defend */
	public int getDefend() {
		return Meriam.defend;
	}

	/* Getter energy */
	public int getEnergy() {
		return Meriam.energy;
	}

	/* Getter playable */
	public boolean isPlayable() {
		return Meriam.playable;
	}

	/* Getter health */
	public int getHealth() {
		return health;
	}
}
