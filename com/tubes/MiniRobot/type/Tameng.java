package com.tubes.MiniRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.ENERGY;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.PRICE;
import com.tubes.MiniRobot.DefenderMiniRobot;
import com.tubes.MiniRobot.MiniRobot;

public class Tameng extends DefenderMiniRobot {
	public static final int ID = 3;
	public static int attack = ATTACK.NULL;
	public static int cost = PRICE.CHEAP;
	public static int defend = HEALTH.HIGH;
	public static int energy = ENERGY.CHEAP;

	private static boolean playable;
	private int health = Tameng.defend;

	public void setHealth(int health) {
		this.health = health;
	}

	public Tameng() {
		super(0, 0);
		MiniRobot.types.add(information);
	}

	public Tameng(int x, int y) {
		super(x, y);
		MiniRobot.types.add(information);
	}

	public static void printInfo() {
		new Tameng().printInformation();
	}

	/* Getter id */
	public static int getId() {
		return Tameng.ID;
	}

	/* Getter attack */
	public int getAttack() {
		return Tameng.attack;
	}

	/* Getter cost */
	public int getCost() {
		return Tameng.cost;
	}

	/* Getter defend */
	public int getDefend() {
		return Tameng.defend;
	}

	/* Getter energy */
	public int getEnergy() {
		return Tameng.energy;
	}

	/* Getter playable */
	public boolean isPlayable() {
		return Tameng.playable;
	}

	/* Getter health */
	public int getHealth() {
		return health;
	}
}
