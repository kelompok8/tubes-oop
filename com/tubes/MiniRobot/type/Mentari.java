package com.tubes.MiniRobot.type;

import com.tubes.Core.CONTROL.ATTACK;
import com.tubes.Core.CONTROL.ENERGY;
import com.tubes.Core.CONTROL.HEALTH;
import com.tubes.Core.CONTROL.PRICE;
import com.tubes.MiniRobot.EnergizerMiniRobot;
import com.tubes.MiniRobot.MiniRobot;

public class Mentari extends EnergizerMiniRobot {
	public static final int ID = 2;
	public static int attack = ATTACK.NULL;
	public static int cost = PRICE.CHEAP;
	public static int defend = HEALTH.LOW;
	public static int energy = ENERGY.CHEAP;

	private static boolean playable;
	private int health = Mentari.defend;

	public void setHealth(int health) {
		this.health = health;
	}

	public Mentari() {
		super(0, 0);
		MiniRobot.types.add(information);
	}

	public Mentari(int x, int y) {
		super(x, y);
		MiniRobot.types.add(information);
	}

	public static void printInfo() {
		new Mentari().printInformation();
	}

	/* Getter id */
	public static int getId() {
		return Mentari.ID;
	}

	/* Getter attack */
	public int getAttack() {
		return Mentari.attack;
	}

	/* Getter cost */
	public int getCost() {
		return Mentari.cost;
	}

	/* Getter defend */
	public int getDefend() {
		return Mentari.defend;
	}

	/* Getter energy */
	public int getEnergy() {
		return Mentari.energy;
	}

	/* Getter playable */
	public boolean isPlayable() {
		return Mentari.playable;
	}

	/* Getter health */
	public int getHealth() {
		return health;
	}
}