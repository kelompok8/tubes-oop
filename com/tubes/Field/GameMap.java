package com.tubes.Field;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tubes.CLI.Konsol;
import com.tubes.Core.CONTROL;

/** Class Singleton yang hanya membuat satu Instansiasi dari **/

public class GameMap implements Cloneable {
	private static Map<Integer, GameMap> gamemap = new HashMap<Integer, GameMap>();

	private char[][] mapChar;

	private int lengthOfMap;
	private int heightOfMap;

	private int level;

	private GameMap(int level) {
		super();
		String[] mapTerrain;
		this.level = level;
		mapTerrain = MapArrayProvider.readLines("level"
				+ Integer.toString(level) + ".txt");
		mapChar = new char[mapTerrain.length][];
		for (int i = 0; i < mapTerrain.length; i++) {
			mapTerrain[i] = StringUtils.replaceChars(mapTerrain[i], "a", " ");
			mapTerrain[i] = StringUtils.replaceChars(mapTerrain[i], "g", "_");
			mapChar[i] = mapTerrain[i].toCharArray();
		}

		heightOfMap = mapTerrain.length;
		lengthOfMap = mapTerrain[0].length();

	}

	public static GameMap getMap(int level) {

		if (GameMap.gamemap.get(level) == null)
			GameMap.gamemap.put(level, new GameMap(level));

		return GameMap.gamemap.get(level);

	}

	public char[][] getMapChar() {

		return MapArrayProvider.util.cloneMapChar(mapChar);
	}

	public void dprint() {

		for (int y = 0; y < heightOfMap; y++) {
			for (int x = 0; x < lengthOfMap; x++)
				System.out.print(StringUtils.repeat(mapChar[y][x],
						CONTROL.H_SIZE));
			System.out.println(StringUtils.repeat("\n", CONTROL.V_SIZE - 1));
		}

	}

	public int getLengthOfMap() {
		return lengthOfMap;
	}

	public int getHeightOfMap() {
		return heightOfMap;
	}

	public int getLevel() {
		return level;
	}

	public void dprint(String prefix) {

		for (int y = 0; y < heightOfMap; y++) {
			System.out.print(prefix);
			for (int x = 0; x < lengthOfMap; x++)
				System.out.print(StringUtils.repeat(mapChar[y][x],
						CONTROL.H_SIZE));
			System.out.println(StringUtils.repeat(prefix + "\n",
					CONTROL.V_SIZE - 1));
		}

	}

	public void dprint(String prefix, String suffix) {

		for (int y = 0; y < heightOfMap; y++) {
			System.out.print(prefix);
			for (int x = 0; x < lengthOfMap; x++)
				System.out.print(StringUtils.repeat(mapChar[y][x],
						CONTROL.H_SIZE));
			System.out.print(StringUtils.repeat(suffix + "\n", CONTROL.V_SIZE));
		}

	}

	public void draw() throws ArrayIndexOutOfBoundsException {
		draw(false);
	}

	public void draw(boolean debug) throws ArrayIndexOutOfBoundsException {
		util.draw(getMapChar(), debug, level);

	}

	public static class util {
		static void draw(char[][] mapChar, boolean debug, int level) {
			int heightOfMap = mapChar.length;
			int lengthOfMap = mapChar[0].length;
			/** Logger TODO **/
			// System.out.println("GameMap.draw()");

			/** Header **/
			System.out.println(StringUtils.repeat("=", CONTROL.H_SIZE
					* lengthOfMap));

			/** Header **/
			if (debug) {

				for (int i = 0; i < lengthOfMap; i++)
					System.out.append(StringUtils.center(Integer.toString(i),
							CONTROL.H_SIZE));
				System.out.println("Char Index");
			}
			for (int i = 0; i < lengthOfMap; i++)
				System.out.append(StringUtils.center(
						Integer.toString(lengthOfMap - i), CONTROL.H_SIZE));
			System.out.println("X");
			System.out.println(StringUtils.repeat("=", CONTROL.H_SIZE
					* lengthOfMap));

			for (int y = 0; y < heightOfMap; y++) {
				/** Left Pad **/
				System.out.print("");

				/** UTAMA! **/
				for (int x = 0; x < lengthOfMap; x++) {

					if (mapChar[y][x] != ' ' && mapChar[y][x] != '_')
						Konsol.color(Color.RED);
					if (GameMap.getMap(level).mapChar[y][x] == '_')
						Konsol.color(Color.WHITE);
					System.out.print(StringUtils.repeat(mapChar[y][x],
							CONTROL.H_SIZE));
					Konsol.reset();
				}

				/** Right Pad **/
				if (debug)
					System.out.print(StringUtils.repeat("| "
							+ (heightOfMap - y) + " " + y + "\n", 1));
				else
					System.out.print(StringUtils.repeat("| "
							+ (heightOfMap - y) + "\n", 1));
				System.out.print(StringUtils.repeat(
						StringUtils.leftPad("| \n", (lengthOfMap + 1)
								* CONTROL.H_SIZE), CONTROL.V_SIZE - 1));
			}

			/** Footer **/
			System.out.println(StringUtils.repeat("=", CONTROL.H_SIZE
					* lengthOfMap));
		}
	}

	public static void main(String[] args) {

		GameMap.getMap(1);
		GameMap.getMap(1).draw();
		System.err.println("meh");

	}

}
