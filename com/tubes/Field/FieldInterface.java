package com.tubes.Field;

import com.tubes.MiniRobot.MiniRobot;

public interface FieldInterface {
	public MiniRobot selectMiniRobot (int x, int y);
	public void addMiniRobot(MiniRobot minirobot);
	public void delMiniRobot(MiniRobot minirobot);
}
